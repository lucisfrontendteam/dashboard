/* ===============================================
Date : 2016-09-01
Description : UI 관련 스크립트 함수 정의
=============================================== */


if (typeof pubUi !== "undefined") {window.alert("pubUi 변수가 중복사용되고 있습니다"); } else {

    var pubUi = {

        /* ==================================================================
            GNB
        ================================================================== */
        gnbAction : function () {
            /* gnb Start */
            var depth1 = $(".dep1");
            var depth2 = $(".dep2>li>a");
            
            depth1.each(function(n){
                $(this).find('>li>a').on('click', function(){
                    depth1.find('>li').removeClass('active');
                    $(this).parent('li').addClass('active');
                    $(this).next('.dep2').find('li:eq(0)>a').trigger('click');
                })
            })
            depth2.each(function(n){
                $(this).on('click', function(){
                    depth2.removeClass('active');
                    $(this).addClass('active');
                })
            })
        },

        /* ==================================================================
            위젯(box) height control
        ================================================================== */
        boxHeightFix : function () {
            Array.max = function (array) {
                return Math.max.apply(Math, array);
            };

            Array.min = function (array) {
                return Math.min.apply(Math, array);
            };
            var values = [];
            var row = $('.js-row-group');
            row.css('height','auto');
            row.each(function (n) {
                $(this).find('.lucis-flipper').each(function () {
                    values.push($(this).innerHeight());
                });
                var absMax = Array.max(values);
                $(this).height(absMax);
                //console.log(values)
                values = [];
            });
        },
        

        /* ==================================================================
            input placeholder
        ================================================================== */
        placeholder : function(){
            var input = $('.js-placeholder').next('input:enabled'); //활성화된 텍스트박스
            var label = $('input:enabled').siblings('.js-placeholder'); //활성화된 텍스트박스의 레이블

            //최초 로딩시 바인딩된 이벤트를 제거
            $('.js-placeholder').next('input').off('focus, blur, change');
            $('input').siblings('.js-placeholder').off('click');

            //이벤트 할당
            input.on({
                focus : function(){
                    $(this).prev('.js-placeholder').css('visibility', 'hidden');
                },
                blur : function(){
                    if ($(this).val() == '' && $(this).html() == '') {
                        $(this).prev('.js-placeholder').css('visibility', 'visible');
                    }
                    else {
                        $(this).prev('.js-placeholder').css('visibility', 'hidden');
                    }
                },
                change : function(){
                    if ($(this).val() == '' && $(this).html() == '') {
                        $(this).prev('.js-placeholder').css('visibility', 'visible');
                    }
                    else {
                        $(this).prev('.js-placeholder').css('visibility', 'hidden');
                    }
                }
            }).blur();
            label.on('click', function(){
                $(this).css('visibility', 'hidden');
                $(this).next('input').focus();
            })
        },


        /* ==================================================================
        문서로딩시 실행함수
        ================================================================== */
        init : function () {
            this.gnbAction(); //GNB 함수
            //this.boxHeightFix(); //그리드 안에 좌 위젯끼리 높이 맞추기 위한 함수
            this.placeholder();
        }


    };

    $(document).ready(function () {
        pubUi.init(); //문서로딩시 실행
        $('[data-toggle="tooltip"]').tooltip()
    });
}