/* ===============================================
Date : 2016-09-01
Description : UI 관련 스크립트 함수 정의
=============================================== */


if (typeof pubUi !== "undefined") {window.alert("pubUi 변수가 중복사용되고 있습니다"); } else {

    var pubUi = {

        /* ==================================================================
            GNB
        ================================================================== */
        gnbAction : function () {
            /* gnb Start */
            var gnb = $(".dep1>li");
            var depth2 = $(".dep2");

            gnb.off('mouseenter focusin').on('mouseenter focusin', function (e) {
                depth2.stop().hide();
                $(this).children('ul').stop().show();
            });
            gnb.off('mouseleave').on('mouseleave', function (e) {
                depth2.stop().attr('style','');
            });
            gnb.find('>a').off('focus').on('focus', function (e) {
                depth2.stop().hide();
                $(this).next('ul').stop().show();
            });
            gnb.off('focusout').on('focusout', function (e) {
                setTimeout(function () {
                    if (gnb.find("a:focus").length == 0) {
                        depth2.stop().attr('style','');
                    }
                }, 0);
            });

        },

        /* ==================================================================
            input placeholder
        ================================================================== */
        placeholder : function(){
            var input = $('.admin-placeholder').next('input:enabled'); //활성화된 텍스트박스
            var label = $('input:enabled').siblings('.admin-placeholder'); //활성화된 텍스트박스의 레이블

            //최초 로딩시 바인딩된 이벤트를 제거
            $('.admin-placeholder').next('input').off('focus, blur, change');
            $('input').siblings('.admin-placeholder').off('click');

            //이벤트 할당
            input.on({
                focus : function(){
                    $(this).prev('.admin-placeholder').css('visibility', 'hidden');
                },
                blur : function(){
                    if ($(this).val() == '' && $(this).html() == '') {
                        $(this).prev('.admin-placeholder').css('visibility', 'visible');
                    }
                    else {
                        $(this).prev('.admin-placeholder').css('visibility', 'hidden');
                    }
                },
                change : function(){
                    if ($(this).val() == '' && $(this).html() == '') {
                        $(this).prev('.admin-placeholder').css('visibility', 'visible');
                    }
                    else {
                        $(this).prev('.admin-placeholder').css('visibility', 'hidden');
                    }
                }
            }).blur();
            label.on('click', function(){
                $(this).css('visibility', 'hidden');
                $(this).next('input').focus();
            })
        },
        
        
        /* ==================================================================
            권한설정 : 전체,쓰기,읽기,수정,삭제 체크박스
        ================================================================== */
        rightSetcheckEvt : function(chkObj){
            var obj = $('.right-set-table tr'); //모든 row
            var objHead = $('.right-set-table thead tr'); //헤더 row
            var objBody = $('.right-set-table tbody tr'); //바디 row

            //동일 row 에서 체크유무에 따라 '전체' 체크를 위한 함수
            var checkAllRow = function(){
                obj.each(function(){
                    var thisObj = $(chkObj).closest('tr');
                    var len = $(this).find('input[type="checkbox"]').not('.check-all').length;
                    if($(this).find('input:checked').not('.check-all').length == len){
                        $(this).find('.check-all').prop('checked', true);
                    }else{
                        $(this).find('.check-all').prop('checked', false);
                    }
                });
            }
            
            //컬럼별 체크박스 종류마다 체크유무에 따라 헤더의 체크박스를 위한 함수
            var checkAllHead = function(){
                var arr = ['.check-all','.check-r','.check-c','.check-u','.check-d'];
                for(i=0; i<arr.length; i++){
                    var len = objBody.find(arr[i]).length;
                    if(objBody.find(arr[i]+':checked').length == len){
                        objHead.find(arr[i]).prop('checked', true);
                    }else{
                        objHead.find(arr[i]).prop('checked', false);
                    }
                }
            }

            //헤더 전체 체크
            objHead.find(chkObj).change(function(){
                if($(this).is('.check-all')){
                    //체크할때
                    if($(this).is(':checked') == true){
                        obj.find('input[type="checkbox"]').prop('checked', true);
                    }else{
                    //해제할때
                        obj.find('input[type="checkbox"]').prop('checked', false);
                    }
                }else{
                    //체크할때
                    if($(this).is(':checked') == true){
                        objBody.find(chkObj).not(':checked').click();//클릭이벤트를 할당함으로 change 이벤트를 감지
                    }else{
                    //해제할때
                        objBody.find(chkObj+':checked').click();
                    }
                }
            });
            
            //전체 row 마다 체크박스 이벤트
            obj.find(chkObj).change(function(){
                //체크할때
                if($(this).is(':checked') == true){
                    if($(this).is('.check-all')){//전체체크할때
                        $(this).closest('tr').find('input[type="checkbox"]').prop('checked', true);; //클릭 타겟의 같은레벨 모두 체크
                    }else{
                        $(this).closest('tr').find('.check-r').prop('checked', true);;//읽기는 기본으로 체크시킨다
                    }
                }else{
                //해제할때
                    if($(this).is('.check-all') || $(this).is('.check-r')){//전체 or 읽기 체크해제할때
                        $(this).closest('tr').find('input[type="checkbox"]').prop('checked', false); //클릭 타겟의 같은레벨 모두 해제
                    }else{
                        $(this).closest('tr').find('.check-all').prop('checked', false); //클릭 타겟의 '전체' 해제
                    }
                }
                checkAllRow(); //동일 row 안의 '전체' 체크유무를 위한 함수실행
                checkAllHead(); //헤더의 체크박스 체크유무를 위한 함수실행
            });
        },


        /* ==================================================================
        문서로딩시 실행함수
        ================================================================== */
        init : function () {
            this.gnbAction(); //GNB 함수
            this.placeholder(); // input placeholder 기능(ie9전용)
        }


    };

    $(document).ready(function () {
        pubUi.init(); //문서로딩시 실행
    });
}